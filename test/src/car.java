import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;


public class car extends JPanel implements ActionListener {

    Timer tm = new Timer(5, this);
    int x = 125, velX = 1;
    int y = 150, velY = 1;
    static int sum = 0, sum2 = 0, sum3 = 0, sum4 = 0;
    //set up map
    File file = new File("res/images/test_v2.png");
    BufferedImage image;

    {
        try {
            image = ImageIO.read(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void paintComponent(Graphics g)
        {
            //set up map
        super.paintComponent(g);
        ImageIcon i = new ImageIcon("res/images/test_v2.png");
        i.paintIcon(this, g, 0, 0);

        g.setColor(Color.BLUE);
        g.fillRect(x,y, 10,10);

        tm.start();
        }
    public static void main(String[] args)
    {
        //set up jframe display
        JFrame jf = new JFrame();
        jf.setTitle("Racing");
        jf.setSize(512,512);

        jf.setVisible(true);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        System.out.println(sum);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }

    public class Car {
        private int up;
        private int down;
        private int left;
        private int right;

        public int up(int up) {
            this.up= up;
            System.out.println("up");
            return up;
        }

        public int down(int down) {
            this.down= down;
            System.out.println("down");
            return down;
        }

        public int left(int left) {
            this.left= left;
            System.out.println("left");
            return left;
        }

        public int right(int right) {
            this.right= right;
            System.out.println("right");
            return right;
        }

    }

}
